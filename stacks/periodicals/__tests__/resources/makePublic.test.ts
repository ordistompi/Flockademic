jest.mock('../../src/services/makePublic', () => ({
  makePublic: jest.fn().mockReturnValue(Promise.resolve({
    creator: {
      identifier: 'Arbitrary account id',
    },
    identifier: 'arbitrary_identifier',
  })),
}));
jest.mock('../../src/services/periodical', () => ({
  fetchPeriodical: jest.fn().mockReturnValue(Promise.resolve({
    creator: {
      identifier: 'Arbitrary account id',
    },
    identifier: 'arbitrary_identifier',
    name: 'Arbitrary name',
  })),
}));
jest.mock('../../../../lib/utils/periodicals', () => ({
  isPeriodicalOwner: jest.fn().mockReturnValue(true),
}));

import { makePublic } from '../../src/resources/makePublic';

const mockContext = {
  body: {
    object: { identifier: 'arbitrary-proposed-slug' },
    targetCollection: { identifier: 'arbitrary-uuid' },
  },
  database: {} as any,
  session: { identifier: 'Arbitrary ID', account: { identifier: 'Arbitrary account ID' } },

  headers: {},
  method: 'POST' as 'POST',
  params: [ '/arbitrary_id/publish', 'arbitrary_id', 'publish' ],
  path: '/arbitrary_id/publish',
  query: null,
};

it('should error when no journal slug was specified', () => {
  const invalidRequest: any = {
    object: {},
    targetCollection: mockContext.body.targetCollection,
  };
  const promise = makePublic({ ...mockContext, body: invalidRequest });

  return expect(promise).rejects.toEqual(new Error('No link to make the journal public at was specified.'));
});

it('should error when an invalid journal slug was specified', () => {
  const invalidRequest: any = {
    object: { identifier: 'Invalid because it is more than 39 characters long' },
    targetCollection: mockContext.body.targetCollection,
  };
  const promise = makePublic({ ...mockContext, body: invalidRequest });

  return expect(promise).rejects.toEqual(new Error('The given journal link is not valid.'));
});

it('should error when the user does not have a session', () => {
  const promise = makePublic({
    ...mockContext,
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when the user does not have an account', () => {
  const promise = makePublic({
    ...mockContext,
    session: { identifier: 'Arbitrary session ID' },
  });

  return expect(promise)
    .rejects.toEqual(new Error('You can only make a journal public if you have created an account.'));
});

it('should error when no journal ID was specified', () => {
  const invalidRequest: any = {
    object: mockContext.body.object,
    targetCollection: {},
  };
  const promise = makePublic({ ...mockContext, body: invalidRequest });

  return expect(promise).rejects.toEqual(new Error('No journal specified to make public.'));
});

it('should error when the given journal could not be found', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = makePublic(mockContext);

  return expect(promise).rejects.toEqual(new Error('Some error'));
});

it('should error when the given journal is not managed by the current user', () => {
  const mockedFetchPeriodical = require.requireMock('../../src/services/periodical').fetchPeriodical;
  mockedFetchPeriodical.mockReturnValueOnce({
    creator: undefined,
    identifier: 'arbitrary_slug',
  });

  const promise = makePublic(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only make your own journals public.'));
});

it('should error when the given journal is managed by someone other than the current user', () => {
  const mockedIsPeriodicalOwner = require.requireMock('../../../../lib/utils/periodicals').isPeriodicalOwner;
  mockedIsPeriodicalOwner.mockReturnValueOnce(false);

  const promise = makePublic(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only make your own journals public.'));
});

it('should error when the given journal does not yet have a valid name', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: mockContext.session.account,
    identifier: 'arbitrary-identifier',
  }));

  const promise = makePublic(mockContext);

  return expect(promise)
    .rejects.toEqual(new Error('The journal has to have a valid name before it can be made public.'));
});

it('should return the slug of a newly published journal', () => {
  const mockedMakePublicService = require.requireMock('../../src/services/makePublic');
  mockedMakePublicService.makePublic.mockReturnValueOnce(Promise.resolve({
    creator: { identifier: 'Some account ID' },
    identifier: 'some-slug',
  }));

  const promise = makePublic({
    ...mockContext,
    body: {
      ...mockContext.body,
      targetCollection: { identifier: 'some_uuid' },
    },
  });

  return expect(promise).resolves.toEqual({
    result: {
      creator: { identifier: 'Some account ID' },
      identifier: 'some-slug',
    },
    targetCollection: { identifier: 'some_uuid' },
  });
});

it('should error when the journal could not be made public, and log it', (done) => {
  const mockedMakePublicService = require.requireMock('../../src/services/makePublic');
  mockedMakePublicService.makePublic.mockReturnValueOnce(Promise.reject(new Error('Some error')));
  console.log = jest.fn();

  const promise = makePublic(mockContext);

  expect(promise)
    .rejects.toEqual(new Error('There was a problem making the journal public, please try again.'));

  setImmediate(() => {
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('Database error:');
    expect(console.log.mock.calls[0][1]).toEqual(new Error('Some error'));

    done();
  });
});
